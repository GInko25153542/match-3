namespace mygame {
    export class Boot extends Phaser.State {
        public preload(): void {
            this.load.crossOrigin = "anonymous";

            this.game.load.image("preloaderBar", "assets/loader_plank.png");
        }

        public create(): void {
            // some basic configutation
            this.game.input.touch.preventDefault = false;
            this.game.stage.backgroundColor = 0x1d1d1d;
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;

            // make this game fullscreen on mobile devices
            if (!this.game.device.desktop) {
                this.game.scale.forceOrientation(true, false);
                this.game.scale.enterIncorrectOrientation.add(() => {
                    document.getElementById("turn").style.display = "block";
                });

                this.game.scale.leaveIncorrectOrientation.add(() => {
                    document.getElementById("turn").style.display = "none";
                });

                const gameElement = document.getElementById("game");

                function onClick() {
                    try {
                        document.body.webkitRequestFullscreen();
                    } catch (e) {
                        // browser doesn't support this
                    }
                    try {
                        document.body.requestFullscreen();
                    } catch (e) {
                        // browser doesn't support this
                    }
                    gameElement.removeEventListener("click", onClick, false);
                }

                gameElement.addEventListener("click", onClick, false);

                this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.game.scale.refresh();
            }

            this.game.state.start("Preloader");
        }
    }
}
