namespace mygame {
    export class BaseGame extends Phaser.State {
        private _bg: Phaser.Sprite;
        private _field: Field;
        private _clock: Clock;
        private _overlay: Phaser.Graphics;

        private _backgroundSound: Phaser.Sound;
        private _audioButton: Phaser.Sprite;

        public create(): void {
            this._backgroundSound = this.game.sound.play("elevator", GameConfig.BACKGROUND_MUSIC_VOULME, true);
            this._bg = this.game.add.sprite(0, 0, "bg");
            this.game.world.addChild(this._bg);
            this._field = new Field(this.game, 0, 0);
            this.game.world.addChild(this._field);
            this._field.position.set(583, 132);
            this._clock = new Clock(this.game, 275, this.game.world.centerY);
            this._clock.scale.set(0.8);
            this._clock.onTimeEnd.add(this.showEndPopup, this);
            this._clock.start(window["roundTime"] * 1000);
            this.game.world.addChild(this._clock);

            this.createAudioToggleButton();
        }

        private createAudioToggleButton(): void {
            this._audioButton = this.game.add.sprite(0, 0, "atlas", "audio");
            this._audioButton.scale.set(0.7);
            this._audioButton.inputEnabled = true;
            this._audioButton.events.onInputOver.add(() => {
                if (this.game.sound.mute) {
                    this._audioButton.loadTexture("atlas", "audio");
                } else {
                    this._audioButton.loadTexture("atlas", "mute");
                }
            }, this);

            this._audioButton.events.onInputOut.add(() => {
                if (this.game.sound.mute) {
                    this._audioButton.loadTexture("atlas", "mute");
                } else {
                    this._audioButton.loadTexture("atlas", "audio");
                }
            }, this);
            this._audioButton.events.onInputDown.add(() => {
                if (this.game.sound.mute) {
                    this._audioButton.loadTexture("atlas", "audio");
                } else {
                    this._audioButton.loadTexture("atlas", "mute");
                }
                this.game.sound.mute = !this.game.sound.mute;
            }, this);
            this._audioButton.position.set(GameConfig.WIDTH - this._audioButton.width, 0);
            this.game.world.addChild(this._audioButton);
        }

        /**
         * It's called to show end game data
         *
         * @private
         * @memberof BaseGame
         */
        private showEndPopup(): void {
            // this._backgroundSound.stop();
            this.game.sound.play("endGame", GameConfig.END_GAME_SOUND_VOLUME, false);
            // this._backgroundSound.onFadeComplete.add(() => {

            // }, this);

            this.game.tweens.removeAll();

            this._field.visible = false;
            this._clock.visible = false;

            this._overlay = this.game.add.graphics(0, 0);
            this.game.world.addChild(this._overlay);
            this._overlay.beginFill(GameConfig.END_GAME_OVERLAY_COLOR, 0.8)
                .drawRect(0, 0, GameConfig.WIDTH, GameConfig.HEIGHT)
                .endFill();
            // this._overlay.alpha = 0;

            const sunraySprite: Phaser.Sprite = this.game.make.sprite(
                this.game.world.centerX, this.game.world.centerY,
                "atlas", "lucksherry_sunray");

            sunraySprite.scale.set(6);
            sunraySprite.anchor.set(0.5);
            sunraySprite.alpha = 0.6;
            this.game.world.addChild(sunraySprite);

            const plank: Phaser.Sprite = this.game.add.sprite(
                this.game.world.centerX, GameConfig.HEIGHT + 1000,
                "atlas", "tutorial_dedula");
            plank.anchor.set(0.5, 1);
            this.game.world.addChild(plank);

            const timeText: Phaser.BitmapText = this.game.add.bitmapText(-256, -300, "font", "", 40);
            timeText.tint = GameConfig.FONT_COLOR;
            timeText.text = `Time: ${window["roundTime"]} sec`;

            plank.addChild(timeText);

            const scoreText: Phaser.BitmapText = this.game.add.bitmapText(-256, -250, "font", "", 40);
            scoreText.tint = GameConfig.FONT_COLOR;
            scoreText.text = `Points: ${this._field.points}`;
            plank.addChild(scoreText);

            this.game.add.tween(plank).to({
                y: GameConfig.HEIGHT,
            }, 500, Phaser.Easing.Sinusoidal.InOut, true);

            this._audioButton.bringToTop();
        }
    }
}
