namespace mygame {
    /**
     * This class handle's loading game resources and showing progress bar
     *
     * @export
     * @class Preloader
     * @extends {Phaser.State}
     */
    export class Preloader extends Phaser.State {
        private _preloadBar: Phaser.Sprite;

        public preload(): void {
            this._preloadBar = this.game.add.sprite(0, 0, "preloaderBar");
            // this._preloadBar.anchor.set(0.5, 0.5);
            this._preloadBar.position.set(this.game.world.centerX - this._preloadBar.width / 2,
                this.game.world.centerY);

            this.load.setPreloadSprite(this._preloadBar);

            this.game.load.image("bg", "assets/bg.jpg");
            this.game.load.atlas("atlas", "assets/atlas.png", "assets/atlas.json");

            // this.game.load.bitmapFont("font", "assets/font_big_gradient.png", "assets/font_big_gradient.fnt");
            this.game.load.bitmapFont("font", "assets/font_small_white.png", "assets/font_small_white.fnt");
            // gem destruction sound
            for (let i: number = 14; i <= 17; i++) {
                this.game.load.audio(i.toString(), `assets/sounds/${i}.mp3`);
            }
            // swap sounds
            this.game.load.audio("22", "assets/sounds/22.mp3");
            this.game.load.audio("23", "assets/sounds/23.mp3");

            this.game.load.audio("endGame", "assets/sounds/5.mp3");
            this.game.load.audio("elevator", "assets/sounds/elevator.mp3");
        }

        public create(): void {
            this.game.state.start("BaseGame", true);
        }

        public shutdown(): void {
            this._preloadBar.destroy();
        }
    }
}
