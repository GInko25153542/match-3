namespace mygame {

    export interface IPoint {
        x: number;
        y: number;
    }

    export interface IPit {
        row: number;
        col: number;
        length: number;
        hasPitAbove: boolean;
    }
    /**
     * Game's field. Handles removing, adding new gem and possible matches
     *
     * @export
     * @class Field
     * @extends {Phaser.Sprite}
     */
    export class Field extends Phaser.Sprite {
        /**
         * background image of the field / board
         */
        private _bg: Phaser.Sprite;
        /**
         * array of the elements thas should stay empty
         */
        private _fieldUpperBound: number[];
        /**
         * 2d array of all field's gems
         */
        private _gemsArray: Gem[][];
        /**
         * flag to allow user click
         */
        private _canClick: boolean;
        /**
         * one of the gems that player selected
         */
        private _selectedGem: Gem;
        /**
         * one of the gems that player selected
         */
        private _pickedGem: Gem;
        /**
         * highlights selected gem
         */
        private _selection: Selection;
        /**
         * map that shows stores elemets to remove
         */
        private _removeMap: number[][];
        /**
         * amount of removed gems
         */
        private _counter: number;
        /**
         * Emitters for each color
         */
        private _particles: Phaser.Particles.Arcade.Emitter[];
        /**
         * graphics for the field mask
         */
        private _maskGraphics: Phaser.Graphics;
        /**
         * container for gems so mask doesn't overlap background
         */
        private _container: Phaser.Sprite;

        constructor(game: Phaser.Game, x: number, y: number) {
            super(game, x, y);

            this._bg = this.game.add.sprite(-12, -12, "atlas", "field");
            this.addChild(this._bg);

            this._container = this.game.add.sprite(0, 0);
            this.addChild(this._container);

            this._gemsArray = [];
            this._canClick = true;
            this._selectedGem = null;

            this.initField();
            this.initParticles();

            this._selection = new Selection(this.game);

            this.addChild(this._selection);

            this._selection.alpha = 0;
            this._selection.data = null;

            this.initMask();

            this._counter = 0;
        }

        /**
         * Amount of player's points
         *
         * @readonly
         * @type {number}
         * @memberof Field
         */
        public get points(): number {
            return this._counter;
        }

        public update(): void {
            this._particles.forEach((p: Phaser.Particles.Arcade.Emitter) => p.update());
        }

        /**
         * Initializes field's mask so gems falling down correcty
         *
         * @private
         * @memberof Field
         */
        private initMask(): void {
            this._maskGraphics = this.game.add.graphics(0, 0);
            this._maskGraphics.beginFill(0xffffff)
                .drawRect(GameConfig.BLOCK_SIZE, 0, 5 * GameConfig.BLOCK_SIZE, GameConfig.BLOCK_SIZE)
                .drawRect(0, GameConfig.BLOCK_SIZE, 7 * GameConfig.BLOCK_SIZE, GameConfig.BLOCK_SIZE * 3)
                .drawRect(GameConfig.BLOCK_SIZE, GameConfig.BLOCK_SIZE * 4, 5 * GameConfig.BLOCK_SIZE,
                    GameConfig.BLOCK_SIZE)
                .endFill();

            this._container.addChild(this._maskGraphics);
            this._container.mask = this._maskGraphics;

        }

        /**
         * Initializes particle emitters for each color
         *
         * @private
         * @memberof Field
         */
        private initParticles(): void {
            this._particles = [];
            for (let i: number = 1; i <= GameConfig.AMOUNT_OF_COLORS; i++) {
                const emitter: Phaser.Particles.Arcade.Emitter = this.game.add.emitter(0, 0, 50);
                emitter.lifespan = 1100;
                this.addChild(emitter);
                emitter.makeParticles("atlas", `particle_${i}`);
                emitter.minParticleSpeed.set(-300, -300);
                emitter.maxParticleSpeed.set(300, 300);
                emitter.setScale(1.2, 0, 1.2, 0, 1100, Phaser.Easing.Quadratic.Out);

                this._particles.push(emitter);
            }
        }

        /**
         * Initializes field
         *
         * @private
         * @memberof Field
         */
        private initField(): void {
            this._fieldUpperBound = GameConfig.FIELD_UPPER_BOUND.slice();
            this._removeMap = [];
            for (let i = 0; i < GameConfig.INITIAL_FIELD.length; i++) {
                this._removeMap.push(new Array(GameConfig.INITIAL_FIELD[i].length));
                this._gemsArray.push([]);

                for (let j = 0; j < GameConfig.INITIAL_FIELD[i].length; j++) {
                    const type: number = GameConfig.INITIAL_FIELD[i][j];

                    if (type > 0) {
                        const x: number = (j + 0.5) * GameConfig.BLOCK_SIZE;
                        const y: number = (i + 0.5) * GameConfig.BLOCK_SIZE;

                        const gem: Gem = new Gem(this.game, x, y, type);
                        gem.row = i;
                        gem.col = j;

                        gem.inputEnabled = true;
                        gem.events.onInputDown.add((obj: Phaser.Sprite, pointer: Phaser.Pointer) => {
                            if (this._canClick) {
                                this.selectGem(gem, pointer);
                            }
                        }, this);

                        this._container.addChild(gem);
                        this._gemsArray[i].push(gem);
                    } else {
                        this._gemsArray[i].push(null);
                    }
                }
            }
        }

        private selectGem(gem: Gem, pointer: Phaser.Pointer): void {
            if (this._selectedGem == null) {
                this._selectedGem = gem;
                this._selection.show(gem.x, gem.y);
            } else if (this._selectedGem.isSame(gem)) {
                this._selection.hide();
                this._selectedGem = null;
            } else if (this._selectedGem.isNext(gem)) {
                this._pickedGem = gem;
                this.swapGems();
                this._selection.hide();
            } else {
                this._selection.hide();
                this._selectedGem = gem;
                this._selection.show(gem.x, gem.y);
            }
        }

        private swapGems(swapBack: boolean = true): void {
            if (swapBack) {
                this.game.sound.play("22", GameConfig.GEM_SWAP_SOUND_VOLUME);
            } else {
                this.game.sound.play("23", GameConfig.GEM_SWAP_SOUND_VOLUME);
            }
            this._canClick = false;
            this._gemsArray[this._selectedGem.row][this._selectedGem.col] = this._pickedGem;
            this._gemsArray[this._pickedGem.row][this._pickedGem.col] = this._selectedGem;

            const p1: IPoint = { x: this._selectedGem.row, y: this._selectedGem.col };
            const p2: IPoint = { x: this._pickedGem.row, y: this._pickedGem.col };
            // TODO: clean up
            const rowSel: number = this._selectedGem.row;
            const colSel: number = this._selectedGem.col;
            const rowPic: number = this._pickedGem.row;
            const colPic: number = this._pickedGem.col;

            this._selectedGem.row = rowPic;
            this._selectedGem.col = colPic;
            this._pickedGem.row = rowSel;
            this._pickedGem.col = colSel;
            // save poistions
            const posPicked: Phaser.Point = new Phaser.Point(
                (colPic + 0.5) * GameConfig.BLOCK_SIZE,
                (rowPic + 0.5) * GameConfig.BLOCK_SIZE);

            const posSelected: Phaser.Point = new Phaser.Point(
                (colSel + 0.5) * GameConfig.BLOCK_SIZE,
                (rowSel + 0.5) * GameConfig.BLOCK_SIZE);

            // add tweens
            const tweenSelected: Phaser.Tween = this.game.add.tween(this._selectedGem).to({
                x: posPicked.x,
                y: posPicked.y,
            }, GameConfig.SWAP_TIME, Phaser.Easing.Sinusoidal.InOut, false);

            const tweenPicked: Phaser.Tween = this.game.add.tween(this._pickedGem).to({
                x: posSelected.x,
                y: posSelected.y,
            }, GameConfig.SWAP_TIME, Phaser.Easing.Sinusoidal.InOut, false);

            tweenPicked.onComplete.add(() => {
                if (swapBack && this.isMatchOnBoard(p1, p2, this._selectedGem.colorID, this._pickedGem.colorID)) {
                    this.handleMatches();
                    this._selectedGem = null;
                    this._pickedGem = null;
                } else if (swapBack) {
                    this.swapGems(false);
                }
            }, this);

            tweenPicked.start();
            tweenSelected.start();

            if (!swapBack) {
                this._canClick = true;
                this._selectedGem = null;
                this._pickedGem = null;
            }
        }

        /**
         * Looks for a match after gem swap
         *
         * @private
         * @param {IPoint} p1 row and column of the first gem
         * @param {IPoint} p2 row and column of the second gem
         * @param {number} colorID1 color of the first gem
         * @param {number} colorID2 color of the seconds gem
         * @returns {boolean} return true if any match exists
         * @memberof Field
         */
        private isMatchOnBoard(p1: IPoint, p2: IPoint, colorID1: number, colorID2: number): boolean {
            return this.isMatch(p1.x, p1.y, colorID2) || this.isMatch(p2.x, p2.y, colorID1);
        }

        /**
         * Check if the current gem has match
         * It's either vertical or horizontal
         * @private
         * @param {number} row row of the gem
         * @param {number} col column of the gem
         * @param {number} colorID color of the gem
         * @returns {boolean} true if match exists
         * @memberof Field
         */
        private isMatch(row: number, col: number, colorID: number): boolean {
            return this.isHorizontal(row, col, colorID) || this.isVertical(row, col, colorID);
        }

        /**
         * Looks for a horizontal match of a given color
         *
         * @private
         * @param {number} row initial row
         * @param {number} col initial column
         * @param {number} colorID gem's color
         * @returns {boolean} true if match exists
         * @memberof Field
         */
        private isHorizontal(row: number, col: number, colorID: number): boolean {
            let start: number;
            let end: number;
            let streak: number = 0;

            for (let i: number = 0; i < 3; i++) {
                streak = 0;
                start = col + i;
                end = start - 2;
                if (start >= this._gemsArray[0].length) {
                    continue;
                }
                if (end < 0) {
                    continue;
                }
                for (let j: number = start; j >= end; j--) {
                    let currentColorID: number = -1;
                    if (this._gemsArray[row][j] != null && this._gemsArray[row][j].colorID > 0) {
                        currentColorID = this._gemsArray[row][j].colorID;
                    }
                    if (currentColorID === colorID) {
                        streak++;
                    }
                }
                if (streak > 2) {
                    return true;
                }
            }
            return false;
        }

        /**
         * Looks for a vertical match of given color
         *
         * @private
         * @param {number} row initial row
         * @param {number} col initial column
         * @param {number} colorID gem's color
         * @returns {boolean} true if match exists
         * @memberof Field
         */
        private isVertical(row: number, col: number, colorID: number): boolean {
            let start: number;
            let end: number;
            let streak: number = 0;

            for (let j = 0; j < 3; j++) {
                streak = 0;
                start = row + j;
                end = start - 2;
                if (start >= this._gemsArray.length) {
                    continue;
                }
                if (end < 0) {
                    continue;
                }
                for (let i: number = start; i >= end; i--) {
                    let currentColorID: number = -1;
                    if (this._gemsArray[i][col] != null && this._gemsArray[i][col].colorID > 0) {
                        currentColorID = this._gemsArray[i][col].colorID;
                    }
                    if (currentColorID === colorID) {
                        streak++;
                    }
                }
                if (streak > 2) {
                    return true;
                }
            }
            return false;
        }

        /**
         * Create remove map to destroy gems
         *
         * @private
         * @memberof Field
         */
        private handleMatches(): void {
            this._removeMap.forEach((x: number[]) => x.fill(0));
            this.handleHorizontalMatches();
            this.handleVerticalMatches();

            this.destroyGems();
        }

        /**
         * Looks for all vertical matches
         *
         * @private
         * @memberof Field
         */
        private handleVerticalMatches(): void {
            for (let i: number = 0; i < this._gemsArray[0].length; i++) {
                let colorStreak: number = 1;
                let currentColorID: number = -100500;
                let startStreak: number = 0;

                for (let j: number = 0; j < this._gemsArray.length; j++) {
                    if (this._gemsArray[j][i] != null && this._gemsArray[j][i].colorID === currentColorID) {
                        colorStreak++;
                    }
                    if (this._gemsArray[j][i] == null || this._gemsArray[j][i].colorID !== currentColorID
                        || j === this._gemsArray.length - 1) {
                            if (colorStreak >= 3) {
                                for (let k: number = 0; k < colorStreak; k++) {
                                    this._removeMap[startStreak + k][i]++;
                                }
                            }
                            startStreak = j;
                            colorStreak = 1;
                            currentColorID = (this._gemsArray[j][i] != null) ? this._gemsArray[j][i].colorID : -(i + j);
                        }

                }
            }
        }

        /**
         * Looks for all horizontal matches
         *
         * @private
         * @memberof Field
         */
        private handleHorizontalMatches(): void {
            for (let i: number = 0; i < this._gemsArray.length; i++) {
                let colorStreak: number = 1;
                let currentColorID: number = -100500;
                let startStreak: number = 0;

                for (let j = 0; j < this._gemsArray[i].length; j++) {
                    if (this._gemsArray[i][j] != null && this._gemsArray[i][j].colorID === currentColorID) {
                        colorStreak++;
                    }
                    if (this._gemsArray[i][j] == null || this._gemsArray[i][j].colorID !== currentColorID
                        || j === this._gemsArray[i].length - 1) {
                        if (colorStreak >= 3) {
                            for (let k: number = 0; k < colorStreak; k++) {
                                this._removeMap[i][startStreak + k]++;
                            }
                        }
                        startStreak = j;
                        colorStreak = 1;
                        currentColorID = (this._gemsArray[i][j] != null) ? this._gemsArray[i][j].colorID : -(i + j);
                    }
                }
            }
        }

        /**
         * Destroyes gem that corrspond to removeMap
         *
         * @private
         * @memberof Field
         */
        private destroyGems(): void {
            // this._destroyGemsCounter = 0;
            this.game.sound.play(this.game.rnd.between(14, 17).toString(), GameConfig.GEM_DESTROY_SOUND_VOLUME);
            let gem: Gem;
            for (let i: number = 0; i < this._removeMap.length; i++) {
                for (let j: number = 0; j < this._removeMap[i].length; j++) {
                    if (this._removeMap[i][j] > 0) {
                        this._counter++;
                        gem = this._gemsArray[i][j];
                        gem.playDestoy();
                        // this._gemEmitter.position.set(gem.x, gem.y);
                        for (let k = 0; k < 7; k++) {
                            this._particles[gem.colorID - 1].emitParticle(gem.x, gem.y);
                        }
                    }
                }
            }

            gem.animations.currentAnim.onComplete.addOnce(() => {
                this.makeGemsFallDown();
            }, this);

        }

        /**
         * Create new gems and starts falling down tweens
         *
         * @private
         * @memberof Field
         */
        private makeGemsFallDown(): void {
            const pits: IPit[] = this.findPits();
            let tween: Phaser.Tween;

            for (let k: number = 0; k < pits.length; k++) {
                const pit: IPit = pits[k];
                let above: IPit;
                let start: number;
                let end: number;

                if (pit.hasPitAbove) {
                    above = pits[k + 1];
                    start = above.row + above.length - pit.length;
                    end = pit.row - start;
                } else {
                    start = 0;
                    end = pit.row - this._fieldUpperBound[pit.col];
                }
                for (let i: number = 0; i < end; i++) {
                    this.swap(pit.row - 1 - i, pit.col, pit.row + pit.length - 1 - i, pit.col);
                }
                if (!pit.hasPitAbove) {
                    for (let i = this._fieldUpperBound[pit.col]; i < this._fieldUpperBound[pit.col] + pit.length; i++) {
                        const gem: Gem = this._gemsArray[i][pit.col];
                        const rnd: number = this.game.rnd.between(1, 5);
                        gem.colorID = rnd;

                        gem.x = (pit.col + 0.5) * GameConfig.BLOCK_SIZE;
                        gem.y = (gem.row - pit.length + 0.5) * GameConfig.BLOCK_SIZE;
                    }
                    start = this._fieldUpperBound[pit.col];
                    end = pit.row + pit.length;
                } else {
                    const len: number = pit.row - (above.row + above.length - pit.length);
                    start = pit.row + pit.length - len;
                    end = pit.row + pit.length;
                }
                for (let i: number = start; i < end; i++) {
                    const gem: Gem = this._gemsArray[i][pit.col];
                    tween = this.game.add.tween(gem).to({
                        y: gem.y + pit.length * GameConfig.BLOCK_SIZE,
                    }, GameConfig.FALLDOWN_TIME, Phaser.Easing.Back.Out, true);
                }
            }
            tween.onComplete.addOnce(() => {
                if (this.checkGlobalMatch()) {
                    this.handleMatches();
                } else if (this.isMoveExists()) {
                    this._canClick = true;
                } else {
                    this.restartBoard();
                }
            }, this);
        }

        /**
         * Looks for possible matches
         *
         * @private
         * @returns {boolean} true if user can make at least one match
         * @memberof Field
         */
        private isMoveExists(): boolean {
            /*
             * This algorithm look for possible patterns such as:
             * ==== 1st ====
             *   x              x
             * x 0 # #  or  # # 0 x
             *   x              x
             * === 2nd ====
             *   #
             * x 0 x
             *   #
             * and inversed vertions of patterns above
             */

            for (let i: number = 0; i < this._gemsArray.length; i++) {
                for (let j: number = 0; j < this._gemsArray[i].length; j++) {

                    if (this.matchPattern(i, j, GameConfig.NECESSARY_POINTS[0], GameConfig.PATTERN_0)) {
                        return true;
                    }

                    if (this.matchPattern(i, j, GameConfig.NECESSARY_POINTS[1], GameConfig.PATTERN_1)) {
                        return true;
                    }

                    if (this.matchPattern(i, j, GameConfig.NECESSARY_POINTS[2], GameConfig.PATTERN_2)) {
                        return true;
                    }

                    if (this.matchPattern(i, j, GameConfig.NECESSARY_POINTS[3], GameConfig.PATTERN_3)) {
                        return true;
                    }
                }
            }

            return false;
        }

        /**
         * Looks for possible pattern. Part of the possible matches looking for algorithm
         *
         * @private
         * @param {number} row Starting row
         * @param {number} col Starting column
         * @param {IPoint} nessecaryPoint nessecary point for the pattern
         * @param {number[][]} restPoint at least one of this points should have the same color so match can exist
         * @returns {boolean} true if possible match exist
         * @memberof Field
         */
        private matchPattern(row: number, col: number, nessecaryPoint: IPoint, restPoint: number[][]): boolean {
            if (this._gemsArray[row][col] == null) {
                return false;
            }
            const currentColorID: number = this._gemsArray[row][col].colorID;
            if (!this.matchType(row + nessecaryPoint.x, col + nessecaryPoint.y, currentColorID)) {
                return false;
            }
            let p: number[];
            for (let i: number = 0; i < restPoint.length; i++) {
                p = restPoint[i];
                if (this.matchType(row + p[0], col + p[1], currentColorID)) {
                    return true;
                }
            }
            return false;

        }

        /**
         * Checks if the gem has the same color as given.
         * It's just a small wrapper to handle additional checkings such as
         * - existance of the gem
         * - boundaries of the row and column
         *
         * @private
         * @param {number} row row of the gem
         * @param {number} col columns of the gem
         * @param {number} colorID color to check
         * @returns {boolean} true if it's the same
         * @memberof Field
         */
        private matchType(row: number, col: number, colorID: number): boolean {
            if (row < 0 || col < 0 || row >= this._gemsArray.length || col >= this._gemsArray[row].length
                || this._gemsArray[row][col] == null) {
                return false;
            }
            return (this._gemsArray[row][col].colorID === colorID);
        }

        /**
         * Resets board to initial state. Called if there is no possible matches
         *
         * @private
         * @memberof Field
         */
        private restartBoard(): void {
            for (let i: number = 0; i < GameConfig.INITIAL_FIELD.length; i++) {
                for (let j: number = 0; j < GameConfig.INITIAL_FIELD[i].length; j++) {
                    if (GameConfig.INITIAL_FIELD[i][j] > 0) {
                        this._gemsArray[i][j].colorID = this.game.rnd.between(1, 5);
                    }
                }
            }
            this._gemsArray[1][0].colorID = 3;
            this._gemsArray[1][2].colorID = 3;
            this._gemsArray[1][3].colorID = 3;
            // this._canClick = true;
            if (this.checkGlobalMatch()) {
                this.handleMatches();
            } else {
                this._canClick = true;
            }
        }

        /**
         * Looks for pits/holes after removing gems
         *
         * @private
         * @returns {IPit[]} all pits/holes on the field
         * @memberof Field
         */
        private findPits(): IPit[] {
            const result: IPit[] = [];
            let id = -1;

            for (let j: number = 0; j < this._gemsArray[0].length; j++) {
                for (let i: number = this._gemsArray.length - 1; i >= 0; i--) {
                    // Going up of array till find > 0
                    if (this._removeMap[i][j] > 0) {
                        let length: number = 1; // total number of empty rows
                        let row: number = i; // starting row of the pit
                        let found: boolean = true; // dirty flag to push IPit if removeMap[i][j] == 0
                        let wasPushed: boolean = false; // dirty flag to push if algorithm reached the top of the column

                        // check the whole column for pits
                        for (let k: number = i - 1; k >= 0; k--) {
                            if (this._removeMap[k][j] > 0) {
                                length++;
                                row = k;
                                found = true;
                                wasPushed = false;
                            } else if (found) {
                                result.push({row, col: j, length, hasPitAbove: false});
                                found = false;
                                wasPushed = true;
                                id = result.length - 2;
                                if (id >= 0 && result[id].col === result[id + 1].col) {
                                    result[id].hasPitAbove = true;
                                }
                            }
                        }
                        if (!wasPushed) {
                            result.push({row, col: j, length, hasPitAbove: false});
                            id = result.length - 2;
                            if (id >= 0 && result[id].col === result[id + 1].col) {
                                result[id].hasPitAbove = true;
                            }
                        }
                        break; // go to the next column
                    }
                }
            }
            return result;
        }

        private swap(x1: number, y1: number, x2: number, y2: number, dx: number = 0): void {
            const temp: Gem = this._gemsArray[x1][y1];
            temp.row = x2 - dx;
            temp.col = y2;

            this._gemsArray[x1][y1] = this._gemsArray[x2][y2];

            this._gemsArray[x1][y1].row = x1 - dx;
            this._gemsArray[x1][y1].col = y1;

            this._gemsArray[x2][y2] = temp;
        }

        /**
         * Checks if there any match on board.
         *
         * @private
         * @returns {boolean} true if it exists
         * @memberof Field
         */
        private checkGlobalMatch(): boolean {
            for (let i: number = 0; i < this._gemsArray.length; i++) {
                for (let j: number = 0; j < this._gemsArray[i].length; j++) {
                    if (this._gemsArray[i][j] != null && this.isMatch(i, j, this._gemsArray[i][j].colorID)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
