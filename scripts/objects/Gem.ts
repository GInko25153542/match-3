namespace mygame {

    /**
     * Colored gem
     *
     * @export
     * @class Gem
     * @extends {Phaser.Sprite}
     */
    export class Gem extends Phaser.Sprite {
        /**
         * row of the gem on the field
         */
        public row: number;
        /**
         * column of the gem on the field
         */
        public col: number;
        /**
         * current color
         */
        private _colorID;
        /**
         * scale down tween when gem is destroyed
         */
        private _destroyTween: Phaser.Tween;

        constructor(game: Phaser.Game, x: number, y: number, colorID: number) {
            super(game, x, y, "atlas", "F_" + colorID);
            this.anchor.set(0.5);
            this._colorID = colorID;

            this._destroyTween = this.game.add.tween(this.scale).to({
                x: GameConfig.GEM_DESTROY_SCALE,
                y: GameConfig.GEM_DESTROY_SCALE,
            }, 100, Phaser.Easing.Sinusoidal.InOut, false);
            // resets scale and plays animation
            this._destroyTween.onComplete.add(() => {
                this.scale.set(1);
                this.play("explosion");
            }, this);

            this.animations.add("explosion", Phaser.Animation.generateFrameNames("e_", 1, 7), 24).onComplete.add(() => {
                this.alpha = 0;
            }, this);
        }

        /**
         * Check two gem if they are the same
         *
         * @param {Gem} other gem to check
         * @returns {boolean} true if it's the same gem
         * @memberof Gem
         */
        public isSame(other: Gem): boolean {
            return this.row === other.row && this.col === other.col;
        }

        /**
         * Check if it's swappable gem, i.e. it's left/right or up/down
         *
         * @param {Gem} other
         * @returns {boolean}
         * @memberof Gem
         */
        public isNext(other: Gem): boolean {
            return Math.abs(this.row - other.row) + Math.abs(this.col - other.col) === 1;
        }

        /**
         * Gem's color
         *
         * @type {number}
         * @memberof Gem
         */
        public get colorID(): number {
            return this._colorID;
        }

        /**
         * Change gem's color
         *
         * @memberof Gem
         */
        public set colorID(val: number) {
            this._colorID = val;
            this.loadTexture("atlas", "F_" + this._colorID);
            this.alpha = 1;
        }

        /**
         * Start destroy tween and animation
         *
         * @memberof Gem
         */
        public playDestoy(): void {
            this._destroyTween.start();
        }
    }
}
