namespace mygame {
    /**
     * Game's clock sprite. Shows time till the end.
     *
     * @export
     * @class Clock
     * @extends {Phaser.Sprite}
     */
    export class Clock extends Phaser.Sprite {
        /**
         * Dispatches when time ends
         */
        public onTimeEnd: Phaser.Signal;
        /**
         * Clock's sprite
         */
        private _clockClosed: Phaser.Sprite;
        /**
         * gprahics for mask
         */
        private _gfx: Phaser.Graphics;
        /**
         * Clock's hour hand sprite
         */
        private _hourHand: Phaser.Sprite;
        /**
         * Clock's minute hand sprite
         */
        private _minuteHand: Phaser.Sprite;
        /**
         * Array of lights to emphesize left time
         */
        private _redLights: Phaser.Sprite[];
        /**
         * Simple sprite to add glass effect
         */
        private _clockLinse: Phaser.Sprite;

        constructor(game: Phaser.Game, x: number, y: number) {
            super(game, x, y, "atlas", "cityClockBG");

            this.anchor.set(0.5, 0.5);

            this._clockClosed = this.game.add.sprite(0, 0, "atlas", "bigClockClose");
            this._clockClosed.anchor.set(0.5, 0.5);
            this.addChild(this._clockClosed);

            this.onTimeEnd = new Phaser.Signal();
            this._gfx = this.game.add.graphics(-this.width / 2, -this.height / 2);
            this._gfx.beginFill(0xfff)
                .drawRoundedRect(0, 0, this.width, this.height, 25)
                .endFill();
            this.addChild(this._gfx);
            this.mask = this._gfx;

            this._redLights = [];
            this.createRedHourLights();

            this._hourHand = this.game.add.sprite(0, 0, "atlas", "hHand");
            this._hourHand.anchor.set(0.5, 0.9);
            this._clockClosed.addChild(this._hourHand);

            this._minuteHand = this.game.add.sprite(0, 0, "atlas", "mHand");
            this._minuteHand.anchor.set(0.5, 0.85);
            this._clockClosed.addChild(this._minuteHand);

            this._clockLinse = this.game.add.sprite(0, 0, "atlas", "clockLinse");
            this._clockLinse.anchor.set(0.5);
            this._clockClosed.addChild(this._clockLinse);
        }

        /**
         * Starts moving minute hand
         *
         * @param {number} time milliseconds till the end
         * @memberof Clock
         */
        public start(time: number): void {
            this.game.add.tween(this._minuteHand).to({
                angle: 360,
            }, time, Phaser.Easing.Linear.None, true).onComplete.addOnce(() => {
                // execute callbacks
                this.onTimeEnd.dispatch();
            }, this);

            // highlights hours when minute hand passes them
            for (let i: number = 1; i <= 12; i++) {
                this.game.time.events.add(time / 12 * i, () => {
                    this._redLights[i % this._redLights.length].alpha = 1;
                }, this);
            }
        }

        /**
         * Initializes red hour lights
         *
         * @private
         * @memberof Clock
         */
        private createRedHourLights(): void {
            const r: number = 171;
            let phi: number;
            let x: number;
            let y: number;
            for (let i = 0; i < 12; i++) {
                phi = 2 * Math.PI / 12 * i;
                x = r * Math.sin(phi);
                y = -r * Math.cos(phi);

                const red: Phaser.Sprite = this.game.add.sprite(x, y, "atlas", "redLight");

                red.alpha = 0;
                red.anchor.set(0.5);

                this._clockClosed.addChild(red);
                this._redLights.push(red);
            }
        }
    }
}
