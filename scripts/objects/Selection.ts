namespace mygame {

    /**
     * Handles showing/hinding of selection that hightlights given gem by cooardinates
     *
     * @export
     * @class Selection
     * @extends {Phaser.Sprite}
     */
    export class Selection extends Phaser.Sprite {
        private _scaleTween: Phaser.Tween;
        constructor(game: Phaser.Game) {
            super(game, 0, 0, "atlas", "selection");
            this.scale.set(0.7);
            this.anchor.set(0.5);
            this.alpha = 0;
        }

        /**
         * Where to show selection on the field
         *
         * @param {number} x
         * @param {number} y
         * @memberof Selection
         */
        public show(x: number, y: number): void {
            this._scaleTween = this.game.add.tween(this.scale).to({
                x: 0.6,
                y: 0.6,
            }, 750, Phaser.Easing.Sinusoidal.InOut, true, 0, -1, true);
            this.position.set(x, y);
            this.alpha = 1;
        }

        /**
         * Hide this selection
         *
         * @memberof Selection
         */
        public hide(): void {
            this.alpha = 0;
            // stopping tween === killing it.
            this._scaleTween.stop();
            this.scale.set(0.7);
        }
    }
}
