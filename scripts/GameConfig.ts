namespace mygame {
    export class GameConfig {
        public static readonly GEM_DESTROY_SOUND_VOLUME: number = 0.4;
        public static readonly GEM_SWAP_SOUND_VOLUME: number = 0.4;
        public static readonly END_GAME_SOUND_VOLUME: number = 0.3;
        public static readonly BACKGROUND_MUSIC_VOULME: number = 0.2;
        /**
         * Point to find possible matches
         */
        public static readonly NECESSARY_POINTS: IPoint[] = [
            {x: 1, y: 0},
            {x: 2, y: 0},
            {x: 0, y: 1},
            {x: 0, y: 2},
        ];
        /**
         * Possible patterns
         */
        public static readonly PATTERN_0: number[][] = [
            [-2, 0], [-1, -1], [-1, 1], [2, -1], [2, 1], [3, 0],
        ];
        public static readonly PATTERN_1: number[][] = [
            [1, -1], [1, 1],
        ];
        public static readonly PATTERN_2: number[][] = [
            [0, -2], [-1, -1], [1, -1], [-1, 2], [1, 2], [0, 3],
        ];
        public static readonly PATTERN_3: number[][] = [
            [-1, 1], [1, 1],
        ];
        /**
         * Color of the end game text
         */
        public static readonly FONT_COLOR: number = 0x733601;
        /**
         * Color of the overlay
         */
        public static readonly END_GAME_OVERLAY_COLOR: number = 0x251d1a;
        /**
         * Size of the field's square
         */
        public static readonly BLOCK_SIZE: number = 93;
        /**
         * Width of the game
         */
        public static readonly WIDTH: number = 1280;
        /**
         * Height of the game
         */
        public static readonly HEIGHT: number = 720;
        /**
         * Time for swapping gems
         */
        public static readonly SWAP_TIME: number = 250;
        /**
         * Time for falling down gems
         */
        public static readonly FALLDOWN_TIME: number = 750;
        /**
         * Shrinking value of the gems when they are being destroyed
         */
        public static readonly GEM_DESTROY_SCALE: number = 0.15;
        /**
         * Total amount of different colors/gems
         */
        public static readonly AMOUNT_OF_COLORS: number = 5;
        /**
         * Starting field
         */
        public static readonly INITIAL_FIELD: number[][] = [
            [0, 5, 5, 2, 4, 1, 0],
            [2, 3, 1, 2, 1, 1, 5],
            [3, 4, 2, 4, 4, 5, 1],
            [4, 3, 5, 2, 4, 5, 1],
            [0, 5, 5, 2, 5, 4, 0],
        ];
        // public static readonly INITIAL_FIELD: number[][] = [
        //     [0, 1, 2, 3, 4, 5, 0],
        //     [2, 2, 3, 4, 5, 1, 2],
        //     [3, 4, 5, 1, 2, 3, 4],
        //     [5, 1, 2, 3, 4, 5, 1],
        //     [0, 2, 3, 4, 5, 1, 0],
        // ];
        /**
         * Upper conseqent zeroes where elemets can't placed
         */
        public static readonly FIELD_UPPER_BOUND: number[] = [1, 0, 0, 0, 0, 0, 1];

        // public static readonly RANDOM_NUMBERS: number[] = [5,2,1,3,5];

        // TODO: remove this
        // public static enableDrag(s: any): void {
        //     s.inputEnabled = true;
        //     s.input.enableDrag();
        //     s.events.onDragUpdate.add(() => {
        //         console.log(s.x, s.y);
        //     });
        // }

        // public static getRandomNumber(): number {
        //     if (GameConfig.RANDOM_NUMBERS.length > 0) {
        //         return GameConfig.RANDOM_NUMBERS.shift();
        //     } else {
        //         return Math.floor(Math.random() * 5) + 1;
        //     }
        // }
    }
}
