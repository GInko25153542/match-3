namespace mygame {
    export class Game extends Phaser.Game {
        constructor() {
            super(GameConfig.WIDTH, GameConfig.HEIGHT, Phaser.AUTO, "game");

            this.state.add("Boot", Boot, false);
            this.state.add("Preloader", Preloader, false);
            this.state.add("BaseGame", BaseGame, false);

            this.state.start("Boot");
        }
    }
}
